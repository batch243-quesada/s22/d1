// console.log('Hello, World');

// Array Manipulation using methods

// Mutator Methods are functions that mutate or change an array after they're created
// These methods manipulate the original array performing various tasks such as adding and removing elements

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon fruit'];

console.log(fruits);

// push - add elements at the end of the array and returns the present length of the array
let fruitsLength = fruits.push('Mango', 'Avocado');
console.log(fruits);
console.log(fruitsLength);

// pop - removes and returns the last element
let fruitRemove = fruits.pop();
console.log(fruits);
console.log(fruitRemove);


// unshift - add elements at the beginning and returns the present length of the array
let fruitUnshift = fruits.unshift('Lime');
console.log(fruits);
console.log(fruitUnshift);

// shift - removes an element at the beginning and returns the removed element
let fruitShift = fruits.shift('Lime');
console.log(fruits);
console.log(fruitShift);

// splice - simultaneously removes elements from a specified index number and adds elements
// arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);
console.log(fruits.splice(0, 2, 'Mojito', 'Lime'));
console.log(fruits);

fruits.splice(3, 0, 'Cherry');
console.log(fruits);

// sort - rearrange the array elements in alphanumeric order
console.log(fruits.sort());

// reverse - reverses an array
// retunrs the reversed array
console.log(fruits.reverse());
console.log(fruits);


// Non Mutator Methods
// functions that do not modify or change an array after they're created
// These methods do not manipulate the original array performing various tasks such as returning elements from an array and combining and printing the output

let countries = ['US', 'PH', 'CA', 'SG', 'TH', 'PH', 'FR', 'DE', 'PH'];
// indexOF() - returns the index number of the first matching element found in an array. If no match was found, the result will be -1. The search process will be done from first element proceeding to the last element.
// arrayName.indexOf(searchValue);
// arrayName.indexOf(serachValue, startingIndex);
console.log(countries.indexOf('PH')); // logs 1 only (targets the very first kind)
console.log(countries.indexOf('BR')); // logs -1
console.log(countries.indexOf('PH', 2));

// lastIndexOf() - returns the last occurence of an element found in an array. The search process will be done from last element proceeding to the first element.
console.log(countries.lastIndexOf('PH'));
console.log(countries.lastIndexOf('BR'));
console.log(countries.lastIndexOf('PH', 2));

// slice()
// slices from an array and returns a new array
// arrayName.slice(startingIndex);
// arrayName.slice(startingIndex, startOfExcludedIndex);
console.log(countries.slice(2));
console.log(countries.slice(2, 5));
console.log(countries);

console.log(countries.slice(-3,));
console.log(countries.slice(-3, -1));

// toString()
// retunrs an array as string separated by commas
console.log(countries.toString());

// concat()
// combines arrays and returns the combined result
let arrayA = ['drink HTML', 'eat JavaScript'];
let arrayB = ['inhale CSS', 'breathe SASS'];
let arrayC = ['get git', 'be node'];

let arrays = arrayB.concat(arrayA);
console.log(arrays);

console.log(arrayA.concat(arrayC, arrayB, arrayC));

// combine arrays with elements
let combineArrayWithElements = arrayA.concat('smell express', 'throw react');
console.log(combineArrayWithElements);
console.log(arrayA);

// join() - returns an array as string separated by specified separator string
let users = ['John', 'Jane', 'Joe', 'Robert'];
console.log(users.join());
console.log(users.join(' '));

///// ITERATIONS /////

// forEach()
// similar to a for loop that iterates on each of array element
// for each element in the array, the function in the foreach method will be ran.
// no returned value
// arrayName.forEach(function(individualElement) {}

console.log(arrays);
arrays.forEach(function(task) {
	console.log(task);
});

// example
let filteredTasks = [];
let arraysArrays = arrays.forEach(task => {
	if (task.length > 10) {
		filteredTasks.push(task);
	}

});

console.log(filteredTasks);
console.log(arraysArrays); // returns undefined


// map()
// iterates on each element and returns new array with different values depending on the result of the function's operations
// returns a value
// let/const resultArray = arrayName.map(function(elements) {statements; reutrn; });
let numbers = [1, 2, 3, 4, 5];
let numberMap = numbers.map(number => number * number);
console.log(numbers);
console.log(numberMap);

// every()
// checks if all elements in an array meet the given condition
// this is useful validating data stored in arrays esp when dealing with large amounts of data
// will return true value if all elements meet the condition and false if otherwise
// let/const resultArray = arrayName.every(function(element) { return expression/condition; });
let allValid = numbers.every(number => number < 3);
console.log(allValid);

// some()
// checks if at least one element in the array meets the given condition
// returns a true value if the condition is met, false if otherwise
// let/const reulstArray = arrayName.some(function(elements) {return expression/condition});
console.log(numbers);

let someValid = numbers.some(number => number < 2);
console.log(someValid);

// filter()
// returns new array that contains the elements which meets a given condition
// returns an empty array if no element/s were found
// let/const resultArray = arrayName.filter(function(element) {retrun expression/condition});
console.log(numbers);

let filterValid = numbers.filter(number => number < 3);
console.log(filterValid);

// includes()
// checks if the argument passed cac be found in the array
// returns a boolean value which can be saved in a variable
// arrayName.includes(argument);
let products = ['phone', 'laptop', 'desktop', 'mouse'];
console.log(products.includes('tv'));

// reduce()
// evaluates elements from left to right and returns/reduces the array into a single value
// let/const resultValue = arrayName.reduce(function(accumulator, currentValue) { return expression/operation});
console.log(numbers);

let total = numbers.reduce((x, y) => {
	console.log(`This is the value of x: ${x}`);
	console.log(`This is the value of y: ${y}`);
	return x - y;
});
console.log(total);